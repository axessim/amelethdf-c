
! header module
module amelethdf
  use ah5_error_m, only : ah5_check
  use ah5_general_m
end module amelethdf


! Alias for lazy person
module ah5
  use amelethdf
end module ah5
