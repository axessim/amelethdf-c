infotypes not implemented in the amelet-hdf C



Infotype                    Read                    Write

Mesh                        OK                      OK
Link                        OK                      OK
OutputRequest               OK                      OK
Volumic material            OK                      OK 
Surfacic material           OK                      OK
Mulitlayer material         OK                      OK
Generator                   OK                      OK
Slot                        OK                      OK
Variable Loc Element        OK                      OK
Cable Design                --                      --
Spice Network list          --                      --
Dipole Cloud                OK                      OK
Lightning                   OK                      OK
PlaneWave                   OK                      OK

Floating types              OK                      OK   (some flavors can be missing)
Dataset                     OK                      OK
Interaction surface         OK                      OK























