#include "ah5_c_phmodel.h"
#include "ah5_log.h"

/// Read a first order filter. This method is intended to be used to read
/// the permittivity or permeability of a Debye or Lorentz material.
///
/// @param[in] file_id The file.
/// @param[in] path The path of the filters.
/// @param[out] limit The limit value.
/// @param[out] stat The static value.
/// @param[out] nb_filters The number of filters.
/// @param[out] filters The values of the filters.
///
/// @return AH5_TRUE successfull read.
///
char _read_first_order_filter(
    hid_t file_id, const char* path,
    float* limit, float* stat, hsize_t* nb_filters, float** filters) {
  char rdata = AH5_TRUE;
  char* path2 = NULL;
  int nb_dims;
  hsize_t dims[2] = {1, 1};
  H5T_class_t type_class;
  char datasetok = AH5_FALSE;
  size_t length;

  if (!AH5_read_flt_attr(file_id, path, AH5_A_ER_LIMIT, limit))
    rdata = AH5_FALSE;
  if (!AH5_read_flt_attr(file_id, path, AH5_A_ER_STATIC, stat))
    rdata = AH5_FALSE;

  path2 = malloc((strlen(path) + strlen(AH5_G_LIST_OF_FUNCTIONS) + 1) * sizeof(*path2));
  strcpy(path2, path);
  strcat(path2, AH5_G_LIST_OF_FUNCTIONS);
  if (AH5_path_valid(file_id, path2) && rdata)
    if (H5LTget_dataset_ndims(file_id, path2, &nb_dims) >= 0)
      if (nb_dims == 2)
        if (H5LTget_dataset_info(file_id, path2, dims, &type_class, &length) >= 0)
          if (type_class == H5T_FLOAT && length == 4)
            if (AH5_read_flt_dataset(file_id, path2, dims[0]*dims[1], filters)) {
              *nb_filters = dims[0];
              datasetok = AH5_TRUE;
            }
  free(path2);

  if (!datasetok) {
    AH5_log_error("The values of the first-order filters in '%s' cannot be readed.", path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


char AH5_read_phm_vimp (hid_t file_id, const char *path, AH5_material_prop_t *material_prop)
{
  char rdata = AH5_TRUE;
  char* buf = NULL;

  if (AH5_path_valid(file_id, path))
  {
    if (strcmp(AH5_get_name_from_path(path),"relativePermittivity") == 0
        || strcmp(AH5_get_name_from_path(path),"relativePermeability") == 0)
    {
      /* relative permittivity, relative permeability */
      if (AH5_read_str_attr(file_id, path, AH5_A_FLOATING_TYPE, &buf))
      {
        /* @floatingType = singleComplex/arraySet/generalRationalFunction */
        if (strcmp(buf, AH5_V_SINGLE_COMPLEX) == 0)
        {
          material_prop->type = MP_SINGLE_COMPLEX;
          if (!AH5_read_ft_singlecomplex(file_id, path, &(material_prop->data.singlecomplex)))
            rdata = AH5_FALSE;
        }
        else if (strcmp(buf, AH5_V_ARRAYSET) == 0)
        {
          material_prop->type = MP_ARRAYSET;
          if (!AH5_read_ft_arrayset(file_id, path, &(material_prop->data.arrayset)))
            rdata = AH5_FALSE;
        }
        else if (strcmp(buf, AH5_V_GENERAL_RATIONAL_FUNCTION) == 0)
        {
          material_prop->type = MP_GENERAL_RATIONAL_FUNCTION;
          if (!AH5_read_ft_generalrationalfunction(file_id, path,
              &(material_prop->data.generalrationalfunction)))
            rdata = AH5_FALSE;
        }
        else
        {
          AH5_print_err_inv_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_FLOATING_TYPE);
          rdata = AH5_FALSE;
        }
      }
      else if (AH5_read_str_attr(file_id, path, AH5_A_TYPE, &buf))
      {
        /* @type = debye/lorentz */
        if (strcmp(buf, AH5_V_DEBYE) == 0) {
          material_prop->type = MP_DEBYE;
          rdata = _read_first_order_filter(
              file_id, path, &material_prop->data.debye.limit, &material_prop->data.debye.stat,
              &material_prop->data.debye.nb_gtau, &material_prop->data.debye.gtau);
        } else if (strcmp(buf, AH5_V_LORENTZ) == 0) {
          material_prop->type = MP_LORENTZ;
          rdata = _read_first_order_filter(
              file_id, path, &material_prop->data.lorentz.limit, &material_prop->data.lorentz.stat,
              &material_prop->data.lorentz.nb_god, &material_prop->data.lorentz.god);
        } else {
          AH5_print_err_inv_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_TYPE);
          rdata = AH5_FALSE;
        }
      }
      else
      {
        printf("***** ERROR(%s): Missing attribute \"floatingType\" or \"type\" in \"%s\". *****\n\n",
               AH5_C_PHYSICAL_MODEL, path);
        rdata = AH5_FALSE;
      }
    }
    else
    {
      /* electric conductivity, magnetic conductivity */
      if (AH5_read_str_attr(file_id, path, AH5_A_FLOATING_TYPE, &buf))
      {
        if (strcmp(buf, AH5_V_SINGLE_REAL) == 0)
        {
          if (AH5_read_ft_singlereal(file_id, path, &(material_prop->data.singlereal)))
            material_prop->type = MP_SINGLE_REAL;
          else
            rdata = AH5_FALSE;
        }
        else if (strcmp(buf, AH5_V_ARRAYSET) == 0)
        {
          if (AH5_read_ft_arrayset(file_id, path, &(material_prop->data.arrayset)))
            material_prop->type = MP_ARRAYSET;
          else
            rdata = AH5_FALSE;
        }
        else
        {
          AH5_print_err_inv_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_FLOATING_TYPE);
          rdata = AH5_FALSE;
        }
      }
      else
      {
        AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_FLOATING_TYPE);
        rdata = AH5_FALSE;
      }
    }
    free(buf);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}

// Read instance in physicalModel/volume
char AH5_read_phm_volume_instance (hid_t file_id, const char *path,
                                   AH5_volume_instance_t *volume_instance)
{
  const size_t path_len = strlen(path);
  char *path2, rdata = AH5_TRUE;
  /*    char mandatory[][AH5_ATTR_LENGTH] = {}; */

  volume_instance->path = strdup(path);
  volume_instance->opt_attrs.instances = NULL;
  volume_instance->relative_permittivity.type = MP_INVALID;
  volume_instance->relative_permeability.type = MP_INVALID;
  volume_instance->electric_conductivity.type = MP_INVALID;
  volume_instance->magnetic_conductivity.type = MP_INVALID;
  volume_instance->volumetric_mass_density = AH5_V_VOLUMETRIC_MASS_DENSITY_UNDEFINE;

  path2 = malloc((strlen(path) + 1) * sizeof(*path2));

  if (AH5_path_valid(file_id, path) && path2)
  {
    AH5_read_opt_attrs(file_id, path, &(volume_instance->opt_attrs), NULL, 0);
    path2 = realloc(path2, (path_len + strlen(AH5_G_RELATIVE_PERMITTIVITY) + 1) * sizeof(*path2));
    strncpy(path2, path, path_len + 1);
    strncat(path2, AH5_G_RELATIVE_PERMITTIVITY, strlen(AH5_G_RELATIVE_PERMITTIVITY));
    if (!AH5_read_phm_vimp(file_id, path2, &(volume_instance->relative_permittivity)))
      rdata = AH5_FALSE;

    path2 = realloc(path2, (path_len + strlen(AH5_G_RELATIVE_PERMEABILITY) + 1) * sizeof(*path2));
    strncpy(path2, path, path_len + 1);
    strncat(path2, AH5_G_RELATIVE_PERMEABILITY, strlen(AH5_G_RELATIVE_PERMEABILITY));
    if (!AH5_read_phm_vimp(file_id, path2, &(volume_instance->relative_permeability)))
      rdata = AH5_FALSE;

    path2 = realloc(path2, (path_len + strlen(AH5_G_ELECTRIC_CONDUCTIVITY) + 1) * sizeof(*path2));
    strncpy(path2, path, path_len + 1);
    strncat(path2, AH5_G_ELECTRIC_CONDUCTIVITY, strlen(AH5_G_ELECTRIC_CONDUCTIVITY));
    if (!AH5_read_phm_vimp(file_id, path2, &(volume_instance->electric_conductivity)))
      rdata = AH5_FALSE;

    path2 = realloc(path2, (path_len + strlen(AH5_G_MAGNETIC_CONDUCTIVITY) + 1) * sizeof(*path2));
    strncpy(path2, path, path_len + 1);
    strncat(path2, AH5_G_MAGNETIC_CONDUCTIVITY, strlen(AH5_G_MAGNETIC_CONDUCTIVITY));
    if (!AH5_read_phm_vimp(file_id, path2, &(volume_instance->magnetic_conductivity)))
      rdata = AH5_FALSE;

    if (!AH5_read_flt_attr(
          file_id, path, AH5_A_VOLUMETRIC_MASS_DENSITY,
          &(volume_instance->volumetric_mass_density)))
    {
      AH5_print_wrn_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_VOLUMETRIC_MASS_DENSITY);
      volume_instance->volumetric_mass_density = AH5_V_VOLUMETRIC_MASS_DENSITY_UNDEFINE;
    }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }

  free(path2);

  return rdata;
}


// Read "thin dielectric layer" values of instance in physicalModel/surface
char AH5_read_phm_surface_instance_tdl (hid_t file_id, const char *path,
                                        AH5_surface_instance_t *surface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_PHYSICAL_MODEL, AH5_A_THICKNESS};
  char rdata = AH5_TRUE;

  if (AH5_path_valid(file_id, path))
  {
    surface_instance->type = S_THIN_DIELECTRIAH5_C_LAYER;
    if(!AH5_read_str_attr(file_id, path, AH5_A_PHYSICAL_MODEL, &(surface_instance->physicalmodel)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_PHYSICAL_MODEL);
      rdata = AH5_FALSE;
    }
    else
    {
       AH5_read_phm_volume_instance(file_id, surface_instance->physicalmodel, &(surface_instance->volume_instance));
    }
    if(!AH5_read_flt_attr(file_id, path, AH5_A_THICKNESS, &(surface_instance->thickness)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_THICKNESS);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(surface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read "SIBC" values of instance in physicalModel/surface
char AH5_read_phm_surface_instance_sibc (hid_t file_id, const char *path,
    AH5_surface_instance_t *surface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_PHYSICAL_MODEL, AH5_A_THICKNESS};
  char rdata = AH5_TRUE;

  if (AH5_path_valid(file_id, path))
  {
    surface_instance->type = S_SIBC;
    if(!AH5_read_str_attr(file_id, path, AH5_A_PHYSICAL_MODEL, &(surface_instance->physicalmodel)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_PHYSICAL_MODEL);
      rdata = AH5_FALSE;
    }
    if(!AH5_read_flt_attr(file_id, path, AH5_A_THICKNESS, &(surface_instance->thickness)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_THICKNESS);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(surface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read "ZS" values of instance in physicalModel/surface
char AH5_read_phm_surface_instance_zs (hid_t file_id, const char *path,
                                       AH5_surface_instance_t *surface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_ZS}, rdata = AH5_TRUE;

  if (AH5_path_valid(file_id, path))
  {
    surface_instance->type = S_ZS;
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZS, &(surface_instance->zs)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZS);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(surface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read "ZSZT" values of instance in physicalModel/surface
char AH5_read_phm_surface_instance_zszt (hid_t file_id, const char *path,
    AH5_surface_instance_t *surface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_ZS, AH5_A_ZT}, rdata = AH5_TRUE;

  if (AH5_path_valid(file_id, path))
  {
    surface_instance->type = S_ZSZT;
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZS, &(surface_instance->zs)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZS);
      rdata = AH5_FALSE;
    }
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZT, &(surface_instance->zt)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZT);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(surface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read "ZSZT2" values of instance in physicalModel/surface
char AH5_read_phm_surface_instance_zszt2 (hid_t file_id, const char *path,
    AH5_surface_instance_t *surface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_ZS1, AH5_A_ZT1, AH5_A_ZS2, AH5_A_ZT2};
  char rdata = AH5_TRUE;

  if (AH5_path_valid(file_id, path))
  {
    surface_instance->type = S_ZSZT2;
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZS1, &(surface_instance->zs1)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZS1);
      rdata = AH5_FALSE;
    }
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZT1, &(surface_instance->zt1)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZT1);
      rdata = AH5_FALSE;
    }
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZS2, &(surface_instance->zs2)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZS2);
      rdata = AH5_FALSE;
    }
    if(!AH5_read_str_attr(file_id, path, AH5_A_ZT2, &(surface_instance->zt2)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_ZT2);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(surface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read instance in physicalModel/surface
char AH5_read_phm_surface_instance (hid_t file_id, const char *path,
                                    AH5_surface_instance_t *surface_instance)
{
  char *temp, rdata = AH5_TRUE;

  surface_instance->path = strdup(path);
  surface_instance->physicalmodel = NULL;
  surface_instance->thickness = 0;
  surface_instance->zs = NULL;
  surface_instance->zt = NULL;
  surface_instance->zs1 = NULL;
  surface_instance->zs2 = NULL;
  surface_instance->zt1 = NULL;
  surface_instance->zt2 = NULL;
  surface_instance->type = S_INVALID;
  surface_instance->opt_attrs.instances = NULL;

  if (AH5_path_valid(file_id, path))
  {
    if (AH5_read_str_attr(file_id, path, AH5_A_TYPE, &temp))
    {
      if (strcmp(temp, AH5_V_THIN_DIELECTRIC_LAYER) == 0)
      {
        if (!AH5_read_phm_surface_instance_tdl(file_id, path, surface_instance))
          rdata = AH5_FALSE;
      }
      else if (strcmp(temp, AH5_V_SIBC) == 0)
      {
        if (!AH5_read_phm_surface_instance_sibc(file_id, path, surface_instance))
          rdata = AH5_FALSE;
      }
      else if (strcmp(temp, AH5_V_ZS) == 0)
      {
        if (!AH5_read_phm_surface_instance_zs(file_id, path, surface_instance))
          rdata = AH5_FALSE;
      }
      else if (strcmp(temp, AH5_V_ZSZT) == 0)
      {
        if (!AH5_read_phm_surface_instance_zszt(file_id, path, surface_instance))
          rdata = AH5_FALSE;
      }
      else if (strcmp(temp, AH5_V_ZSZT2) == 0)
      {
        if (!AH5_read_phm_surface_instance_zszt2(file_id, path, surface_instance))
          rdata = AH5_FALSE;
      }
      else
      {
        AH5_print_wrn_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_TYPE);
        rdata = AH5_FALSE;
      }
      free(temp);
    }
    else
    {
      surface_instance->type = S_INVALID;
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_TYPE);
      rdata = AH5_FALSE;
    }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}

// Read instance in physicalModel/surface
char AH5_read_phm_multilayer_instance (hid_t file_id, const char *path,
                                    AH5_multilayer_instance_t *multilayer_instance)
{
  char rdata = AH5_TRUE;
  hsize_t i;
  char **field_names;
  size_t *field_sizes;
  size_t *field_offsets;
  hsize_t nfields;
  size_t type_size;
  multilayer_field* fields;

  multilayer_instance->path = strdup(path);
  if (AH5_path_valid(file_id, path)) {
      
      if (H5TBget_table_info(file_id, path, &nfields, &(multilayer_instance->nb_layers)) >= 0) {

        if (nfields == 2 && multilayer_instance->nb_layers > 0) {
          field_names = (char **) malloc((size_t) nfields * sizeof(char *));
          field_names[0] = (char *) malloc((size_t) nfields * AH5_TABLE_FIELD_NAME_LENGTH * sizeof(char));
          for (i = 0; i < nfields; i++)
            field_names[i] = field_names[0] + i * AH5_TABLE_FIELD_NAME_LENGTH;
          field_sizes = (size_t *) malloc((size_t ) nfields * sizeof(size_t));
          field_offsets = (size_t *) malloc((size_t) nfields * sizeof(size_t));

          if (H5TBget_field_info(file_id, path, field_names, field_sizes, field_offsets, &type_size) >= 0) {

            fields = (multilayer_field *) malloc((size_t) nfields * sizeof(multilayer_field));
            H5TBread_records(file_id, path, 0, multilayer_instance->nb_layers, type_size, field_offsets, field_sizes, fields);

            multilayer_instance->layers = (AH5_volume_instance_t *) malloc((size_t) multilayer_instance->nb_layers * sizeof(AH5_volume_instance_t));
            multilayer_instance->thicknesses = (float *) malloc((size_t) multilayer_instance->nb_layers * sizeof(float));

            for (i = 0; i < multilayer_instance->nb_layers; i++) {
              multilayer_instance->thicknesses[i] = fields[i].thickness;
              if (!AH5_read_phm_volume_instance(file_id, fields[i].path, multilayer_instance->layers + i))
                rdata = AH5_FALSE;
            }

            free(fields);
          }
          else {
            rdata = AH5_FALSE;
          }
          free(field_names[0]);
          free(field_names);
          free(field_sizes);
          free(field_offsets);   
        }
      }
      else {
        rdata = AH5_FALSE;
      }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read instance in physicalModel/interface
char AH5_read_phm_interface_instance (hid_t file_id, const char *path,
                                      AH5_interface_instance_t *interface_instance)
{
  char mandatory[][AH5_ATTR_LENGTH] = {AH5_A_MEDIUM1, AH5_A_MEDIUM2};
  char rdata = AH5_TRUE;

  interface_instance->path = strdup(path);
  interface_instance->opt_attrs.instances = NULL;
  interface_instance->medium1 = NULL;
  interface_instance->medium2 = NULL;

  if (AH5_path_valid(file_id, path))
  {
    if (!AH5_read_str_attr(file_id, path, AH5_A_MEDIUM1, &(interface_instance->medium1)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_MEDIUM1);
      rdata = AH5_FALSE;
    }
    if (!AH5_read_str_attr(file_id, path, AH5_A_MEDIUM2, &(interface_instance->medium2)))
    {
      AH5_print_err_attr(AH5_C_PHYSICAL_MODEL, path, AH5_A_MEDIUM2);
      rdata = AH5_FALSE;
    }
    AH5_read_opt_attrs(file_id, path, &(interface_instance->opt_attrs), mandatory,
                       sizeof(mandatory)/AH5_ATTR_LENGTH);
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, path);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Read physicalModel category
char AH5_read_physicalmodel (hid_t file_id, AH5_physicalmodel_t *physicalmodel)
{
  char *path, rdata = AH5_TRUE;
  AH5_children_t children;
  hsize_t i;

  physicalmodel->volume_instances = NULL;
  physicalmodel->surface_instances = NULL;
  physicalmodel->interface_instances = NULL;

  if (AH5_path_valid(file_id, AH5_C_PHYSICAL_MODEL))
  {
    path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_VOLUME) + 1) * sizeof(*path));
    strcpy(path, AH5_C_PHYSICAL_MODEL);
    strcat(path, AH5_G_VOLUME);
    children = AH5_read_children_name(file_id, path);
    physicalmodel->nb_volume_instances = children.nb_children;
    free(path);

    if (children.nb_children > 0)
    {
      physicalmodel->volume_instances = (AH5_volume_instance_t *) malloc((size_t) children.nb_children *
                                        sizeof(AH5_volume_instance_t));
      for (i = 0; i < children.nb_children; i++)
      {
        path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_VOLUME)
                       + strlen(children.childnames[i]) + 1) * sizeof(*path));
        strcpy(path, AH5_C_PHYSICAL_MODEL);
        strcat(path, AH5_G_VOLUME);
        strcat(path, children.childnames[i]);
        if (!AH5_read_phm_volume_instance(file_id, path, physicalmodel->volume_instances + i))
          rdata = AH5_FALSE;
        free(children.childnames[i]);
        free(path);
      }
      free(children.childnames);
    }

    path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_SURFACE) + 1) * sizeof(*path));
    strcpy(path, AH5_C_PHYSICAL_MODEL);
    strcat(path, AH5_G_SURFACE);
    children = AH5_read_children_name(file_id, path);
    physicalmodel->nb_surface_instances = children.nb_children;
    free(path);

    if (children.nb_children > 0)
    {
      physicalmodel->surface_instances = (AH5_surface_instance_t *) malloc((
                                           size_t) children.nb_children * sizeof(AH5_surface_instance_t));
      for (i = 0; i < children.nb_children; i++)
      {
        path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_SURFACE)
                       + strlen(children.childnames[i]) + 1) * sizeof(*path));
        strcpy(path, AH5_C_PHYSICAL_MODEL);
        strcat(path, AH5_G_SURFACE);
        strcat(path, children.childnames[i]);
        if (!AH5_read_phm_surface_instance(file_id, path, physicalmodel->surface_instances + i))
          rdata = AH5_FALSE;
        free(children.childnames[i]);
        free(path);
      }
      free(children.childnames);
    }

    path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_MULTILAYER) + 1) * sizeof(*path));
    strcpy(path, AH5_C_PHYSICAL_MODEL);
    strcat(path, AH5_G_MULTILAYER);
    children = AH5_read_children_name(file_id, path);
    physicalmodel->nb_surface_instances = children.nb_children;
    free(path);

    if (children.nb_children > 0)
    {
      physicalmodel->surface_instances = (AH5_surface_instance_t *) malloc((
                                           size_t) children.nb_children * sizeof(AH5_surface_instance_t));
      for (i = 0; i < children.nb_children; i++)
      {
        path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_MULTILAYER)
                       + strlen(children.childnames[i]) + 1) * sizeof(*path));
        strcpy(path, AH5_C_PHYSICAL_MODEL);
        strcat(path, AH5_G_MULTILAYER);
        strcat(path, children.childnames[i]);
        if (!AH5_read_phm_multilayer_instance(file_id, path, physicalmodel->multilayer_instances + i))
          rdata = AH5_FALSE;
        free(children.childnames[i]);
        free(path);
      }
      free(children.childnames);
    }

    path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_INTERFACE) + 1) * sizeof(*path));
    strcpy(path, AH5_C_PHYSICAL_MODEL);
    strcat(path, AH5_G_INTERFACE);
    children = AH5_read_children_name(file_id, path);
    physicalmodel->nb_interface_instances = children.nb_children;
    free(path);

    if (children.nb_children > 0)
    {
      physicalmodel->interface_instances = (AH5_interface_instance_t *) malloc((
                                             size_t) children.nb_children * sizeof(AH5_interface_instance_t));
      for (i = 0; i < children.nb_children; i++)
      {
        path = malloc((strlen(AH5_C_PHYSICAL_MODEL) + strlen(AH5_G_INTERFACE)
                       + strlen(children.childnames[i]) + 1) * sizeof(*path));
        strcpy(path, AH5_C_PHYSICAL_MODEL);
        strcat(path, AH5_G_INTERFACE);
        strcat(path, children.childnames[i]);
        if (!AH5_read_phm_interface_instance(file_id, path, physicalmodel->interface_instances + i))
          rdata = AH5_FALSE;
        free(children.childnames[i]);
        free(path);
      }
      free(children.childnames);
    }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, AH5_C_PHYSICAL_MODEL);
    rdata = AH5_FALSE;
  }
  return rdata;
}

// Read multiport
char AH5_read_multiport (hid_t file_id, const char *path, AH5_multiport_t *multiport)
{
  char rdata = AH5_TRUE;
  char *buf;
  char *read_path;

  if (AH5_path_valid(file_id, path))
  {
    multiport->path = strdup(path);

    if (AH5_read_str_attr(file_id, path, AH5_A_PHYSICAL_NATURE, &buf)) {
      if (strcmp(buf, AH5_A_RESISTANCE) == 0)
      {
        multiport->type = MULTIPORT_RESISTANCE;
        if (AH5_read_flt_attr(file_id, path, AH5_A_VALUE, &(multiport->value)))
          rdata = AH5_TRUE;
      }
      else if (strcmp(buf, AH5_A_INDUCTANCE) == 0)
      {
        multiport->type = MULTIPORT_INDUCTANCE;
        if (AH5_read_flt_attr(file_id, path, AH5_A_VALUE, &(multiport->value)))
          rdata = AH5_TRUE;
      }
      else if (strcmp(buf, AH5_A_CAPACITANCE) == 0)
      {
        multiport->type = MULTIPORT_CAPACITANCE;
        if (AH5_read_flt_attr(file_id, path, AH5_A_VALUE, &(multiport->value)))
          rdata = AH5_TRUE;
      }
      else if (strcmp(buf, AH5_A_IMPEDANCE) == 0)
      {
        multiport->type = MULTIPORT_IMPEDANCE;
        if (AH5_read_flt_attr(file_id, path, AH5_A_VALUE, &(multiport->value)))
          rdata = AH5_TRUE;
      }
      else {
        AH5_log_error("Invalid type for the physical nature", path);
        //Not implemented
        rdata = AH5_FALSE;
      }
    }
    else if (H5Aexists_by_name(file_id, path, AH5_A_TYPE, H5P_DEFAULT) > 0) {
      // No atribute physical nature, try to read type for RLC
        multiport->type = MULTIPORT_RLC;
        AH5_read_int_attr(file_id, path, AH5_A_TYPE, &(multiport->type_rlc));
        AH5_read_str_attr(file_id, path, "R", &read_path);
        AH5_read_flt_attr(file_id, read_path, AH5_A_VALUE, &(multiport->r_value));
        AH5_read_str_attr(file_id, path, "L", &read_path);
        AH5_read_flt_attr(file_id, read_path, AH5_A_VALUE, &(multiport->l_value));
        AH5_read_str_attr(file_id, path, "C", &read_path);
        AH5_read_flt_attr(file_id, read_path, AH5_A_VALUE, &(multiport->c_value));
    }
    else {
      AH5_log_error("Attribute type not present, unknow type of multiport", path);
      //Not implemented
      rdata = AH5_FALSE;
    }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, AH5_C_PHYSICAL_MODEL);
    rdata = AH5_FALSE;
  }
  return rdata;
}

char AH5_read_slot (hid_t file_id, const char *path, AH5_slot_t *slot) {
  char rdata = AH5_TRUE;
  char *buf;

  if (AH5_path_valid(file_id, path)) {
    slot->path = strdup(path);

    if (AH5_read_flt_attr(file_id, path, AH5_A_THICKNESS, &(slot->thickness)) &&
        AH5_read_flt_attr(file_id, path, AH5_A_WIDTH, &(slot->width))) {
      AH5_read_str_attr(file_id, path, AH5_A_MATERIAL_MODEL, &buf);
      AH5_read_phm_volume_instance(file_id, buf, &(slot->material));
    }
    else {
        rdata = AH5_FALSE;
    }
  }
  else
  {
    AH5_print_err_path(AH5_C_PHYSICAL_MODEL, AH5_G_APERTURE);
    rdata = AH5_FALSE;
  }
  return rdata;
}


// Print instance in physicalModel/volume
void AH5_print_phm_volume_instance (const AH5_volume_instance_t *volume_instance, int space)
{
  hsize_t i;

  printf("%*sInstance: %s\n", space, "", AH5_get_name_from_path(volume_instance->path));
  AH5_print_opt_attrs(&(volume_instance->opt_attrs), space + 3);

  /* relative permittivity */
  if (volume_instance->relative_permittivity.type == MP_SINGLE_COMPLEX)
    AH5_print_ft_singlecomplex(&(volume_instance->relative_permittivity.data.singlecomplex), space + 3);
  else if (volume_instance->relative_permittivity.type == MP_GENERAL_RATIONAL_FUNCTION)
    AH5_print_ft_generalrationalfunction(&
                                         (volume_instance->relative_permittivity.data.generalrationalfunction), space + 3);
  else if (volume_instance->relative_permittivity.type == MP_ARRAYSET)
    AH5_print_ft_arrayset(&(volume_instance->relative_permittivity.data.arrayset), space + 3);
  else if (volume_instance->relative_permittivity.type == MP_DEBYE)
  {
    printf("%*s-relativePermittivity (type=debye):\n", space + 3, "");
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_STATIC,
           volume_instance->relative_permittivity.data.debye.stat);
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_LIMIT,
           volume_instance->relative_permittivity.data.debye.limit);
    for (i = 0; i < volume_instance->relative_permittivity.data.debye.nb_gtau; i++)
    {
      printf("%*sId %lu: G=%g tau=%g\n", space + 5, "", (long unsigned) i,
             volume_instance->relative_permittivity.data.debye.gtau[2*i],
             volume_instance->relative_permittivity.data.debye.gtau[2*i+1]);
    }
  }
  else if (volume_instance->relative_permittivity.type == MP_LORENTZ)
  {
    printf("%*s-relativePermittivity (type=lorentz):\n", space + 3, "");
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_STATIC,
           volume_instance->relative_permittivity.data.lorentz.stat);
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_LIMIT,
           volume_instance->relative_permittivity.data.lorentz.limit);
    for (i = 0; i < volume_instance->relative_permittivity.data.lorentz.nb_god; i++)
      printf("%*sId %lu: G=%g omega=%g delta=%g\n", space + 5, "", (long unsigned) i,
             volume_instance->relative_permittivity.data.lorentz.god[3*i],
             volume_instance->relative_permittivity.data.lorentz.god[3*i+1],
             volume_instance->relative_permittivity.data.lorentz.god[3*i+2]);
  }

  /* relative permeability */
  if (volume_instance->relative_permeability.type == MP_SINGLE_COMPLEX)
    AH5_print_ft_singlecomplex(&(volume_instance->relative_permeability.data.singlecomplex), space + 3);
  else if (volume_instance->relative_permeability.type == MP_GENERAL_RATIONAL_FUNCTION)
    AH5_print_ft_generalrationalfunction(&
                                         (volume_instance->relative_permeability.data.generalrationalfunction), space + 3);
  else if (volume_instance->relative_permeability.type == MP_ARRAYSET)
    AH5_print_ft_arrayset(&(volume_instance->relative_permeability.data.arrayset), space + 3);
  else if (volume_instance->relative_permeability.type == MP_DEBYE)
  {
    printf("%*s-relativePermeability (type=debye):\n", space + 3, "");
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_STATIC,
           volume_instance->relative_permeability.data.debye.stat);
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_LIMIT,
           volume_instance->relative_permeability.data.debye.limit);
    for (i = 0; i < volume_instance->relative_permeability.data.debye.nb_gtau; i++)
    {
      printf("%*sId %lu: G=%g tau=%g\n", space + 5, "", (long unsigned) i,
             volume_instance->relative_permeability.data.debye.gtau[2*i],
             volume_instance->relative_permeability.data.debye.gtau[2*i+1]);
    }
  }
  else if (volume_instance->relative_permeability.type == MP_LORENTZ)
  {
    printf("%*s-relativePermeability (type=lorentz):\n", space + 3, "");
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_STATIC,
           volume_instance->relative_permeability.data.lorentz.stat);
    printf("%*s-@%s: %g\n", space + 7, "", AH5_A_ER_LIMIT,
           volume_instance->relative_permeability.data.lorentz.limit);
    for (i = 0; i < volume_instance->relative_permeability.data.lorentz.nb_god; i++)
      printf("%*sId %lu: G=%g omega=%g delta=%g\n", space + 5, "", (long unsigned) i,
             volume_instance->relative_permeability.data.lorentz.god[3*i],
             volume_instance->relative_permeability.data.lorentz.god[3*i+1],
             volume_instance->relative_permeability.data.lorentz.god[3*i+2]);
  }

  /* electric conductivity */
  if (volume_instance->electric_conductivity.type == MP_SINGLE_REAL)
    AH5_print_ft_singlereal(&(volume_instance->electric_conductivity.data.singlereal), space + 3);
  else if (volume_instance->electric_conductivity.type == MP_ARRAYSET)
    AH5_print_ft_arrayset(&(volume_instance->electric_conductivity.data.arrayset), space + 3);

  /* magnetic conductivity */
  if (volume_instance->magnetic_conductivity.type == MP_SINGLE_REAL)
    AH5_print_ft_singlereal(&(volume_instance->magnetic_conductivity.data.singlereal), space + 3);
  else if (volume_instance->magnetic_conductivity.type == MP_ARRAYSET)
    AH5_print_ft_arrayset(&(volume_instance->magnetic_conductivity.data.arrayset), space + 3);
  printf("\n");
}


// Print instance in physicalModel/surface
void AH5_print_phm_surface_instance (const AH5_surface_instance_t *surface_instance, int space)
{
  printf("%*sInstance: %s\n", space, "", AH5_get_name_from_path(surface_instance->path));
  AH5_print_opt_attrs(&(surface_instance->opt_attrs), space + 3);
  switch (surface_instance->type)
  {
  case S_THIN_DIELECTRIAH5_C_LAYER:
    AH5_print_str_attr(AH5_A_PHYSICAL_MODEL, surface_instance->physicalmodel, space + 3);
    AH5_print_flt_attr(AH5_A_THICKNESS, surface_instance->thickness, space + 3);
    break;
  case S_SIBC:
    AH5_print_str_attr(AH5_A_PHYSICAL_MODEL, surface_instance->physicalmodel, space + 3);
    AH5_print_flt_attr(AH5_A_THICKNESS, surface_instance->thickness, space + 3);
    break;
  case S_ZS:
    AH5_print_str_attr(AH5_A_ZS, surface_instance->zs, space + 3);
    break;
  case S_ZSZT:
    AH5_print_str_attr(AH5_A_ZS, surface_instance->zs, space + 3);
    AH5_print_str_attr(AH5_A_ZT, surface_instance->zt, space + 3);
    break;
  case S_ZSZT2:
    AH5_print_str_attr(AH5_A_ZS1, surface_instance->zs1, space + 3);
    AH5_print_str_attr(AH5_A_ZT1, surface_instance->zt1, space + 3);
    AH5_print_str_attr(AH5_A_ZS2, surface_instance->zs2, space + 3);
    AH5_print_str_attr(AH5_A_ZT2, surface_instance->zt2, space + 3);
    break;
  default:
    break;
  }
  printf("\n");
}


// Print instance in physicalModel/interface
void AH5_print_phm_interface_instance (const AH5_interface_instance_t *interface_instance,
                                       int space)
{
  printf("%*sInstance: %s\n", space, "", AH5_get_name_from_path(interface_instance->path));
  AH5_print_opt_attrs(&(interface_instance->opt_attrs), space + 3);
  if (interface_instance->medium1 != NULL)
    AH5_print_str_attr(AH5_A_MEDIUM1, interface_instance->medium1, space + 3);
  if (interface_instance->medium2 != NULL)
    AH5_print_str_attr(AH5_A_MEDIUM2, interface_instance->medium2, space + 3);
  printf("\n");
}


// Print physicalModel category
void AH5_print_physicalmodel (const AH5_physicalmodel_t *physicalmodel)
{
  hsize_t i;

  if (physicalmodel->nb_volume_instances > 0 || physicalmodel->nb_surface_instances > 0
      || physicalmodel->nb_interface_instances > 0)
  {
    printf("##############################  Physical model  ##############################\n\n");
    if (physicalmodel->nb_volume_instances > 0)
    {
      printf("Volume:\n");
      for (i = 0; i < physicalmodel->nb_volume_instances; i++)
        AH5_print_phm_volume_instance(&(physicalmodel->volume_instances[i]), 3);
    }
    if (physicalmodel->nb_surface_instances > 0)
    {
      printf("Surface:\n");
      for (i = 0; i < physicalmodel->nb_surface_instances; i++)
        AH5_print_phm_surface_instance(&(physicalmodel->surface_instances[i]), 3);
    }
    if (physicalmodel->nb_interface_instances > 0)
    {
      printf("Interface:\n");
      for (i = 0; i < physicalmodel->nb_interface_instances; i++)
        AH5_print_phm_interface_instance(&(physicalmodel->interface_instances[i]), 3);
    }
    printf("\n");
  }
}

void AH5_print_multiport (const AH5_multiport_t *multiport) {
  printf("Multiport: %s\n", AH5_get_name_from_path(multiport->path));
  switch (multiport->type)
  {
  case MULTIPORT_CAPACITANCE:
    printf("   Capacitance: value %f\n", multiport->value);
    break;
  case MULTIPORT_RESISTANCE:
    printf("   Resistance: value %f\n", multiport->value);
    break;
  case MULTIPORT_INDUCTANCE:
    printf("   Inductance: value %f\n", multiport->value);
    break;
  case MULTIPORT_IMPEDANCE:
    printf("   Impedance: value %f\n", multiport->value);
    break;
  case MULTIPORT_RLC:
    printf("   RLC: type %d\n", multiport->type_rlc);
    printf("         R value : %f\n", multiport->r_value);
    printf("         L value : %f\n", multiport->l_value);
    printf("         C value : %f\n", multiport->c_value);
    break;

  default:
    printf(" Not implemented");
    break;
  }
}

void AH5_print_slot (const AH5_slot_t *slot) {
  printf("Slot: %s\n", AH5_get_name_from_path(slot->path));
  printf("   Thickness : value %f\n", slot->thickness);
  printf("   Width : value %f\n", slot->width);
  AH5_print_phm_volume_instance(&(slot->material), 3);
}

// Free memory used by desired material property
void AH5_free_phm_vimp (AH5_material_prop_t *material_prop)
{
  if (material_prop->type == MP_SINGLE_REAL)
    AH5_free_ft_singlereal(&(material_prop->data.singlereal));
  else if (material_prop->type == MP_SINGLE_COMPLEX)
    AH5_free_ft_singlecomplex(&(material_prop->data.singlecomplex));
  else if (material_prop->type == MP_GENERAL_RATIONAL_FUNCTION)
    AH5_free_ft_generalrationalfunction(&(material_prop->data.generalrationalfunction));
  else if (material_prop->type == MP_ARRAYSET)
    AH5_free_ft_arrayset(&(material_prop->data.arrayset));
  else if (material_prop->type == MP_DEBYE)
  {
    free(material_prop->data.debye.gtau);
    material_prop->data.debye.gtau = NULL;
  }
  else if (material_prop->type == MP_LORENTZ)
  {
    free(material_prop->data.lorentz.god);
    material_prop->data.lorentz.god = NULL;
  }
  material_prop->type = MP_INVALID;
}


// Free memory used by instance in physicalModel/volume
void AH5_free_phm_volume_instance (AH5_volume_instance_t *volume_instance)
{
  if (volume_instance->path != NULL)
  {
    free(volume_instance->path);
    volume_instance->path = NULL;
  }
  AH5_free_opt_attrs(&(volume_instance->opt_attrs));
  AH5_free_phm_vimp(&(volume_instance->relative_permittivity));
  AH5_free_phm_vimp(&(volume_instance->relative_permeability));
  AH5_free_phm_vimp(&(volume_instance->electric_conductivity));
  AH5_free_phm_vimp(&(volume_instance->magnetic_conductivity));
}

// Free memory used by instance in physicalModel/surface
void AH5_free_phm_surface_instance (AH5_surface_instance_t *surface_instance)
{
  if (surface_instance->path != NULL)
  {
    free(surface_instance->path);
    surface_instance->path = NULL;
  }
  AH5_free_opt_attrs(&(surface_instance->opt_attrs));
  if (surface_instance->physicalmodel != NULL)
  {
    free(surface_instance->physicalmodel);
    surface_instance->physicalmodel = NULL;
  }
  if (surface_instance->zs != NULL)
  {
    free(surface_instance->zs);
    surface_instance->zs = NULL;
  }
  if (surface_instance->zt != NULL)
  {
    free(surface_instance->zt);
    surface_instance->zt = NULL;
  }
  if (surface_instance->zs1 != NULL)
  {
    free(surface_instance->zs1);
    surface_instance->zs1 = NULL;
  }
  if (surface_instance->zt1 != NULL)
  {
    free(surface_instance->zt1);
    surface_instance->zt1 = NULL;
  }
  if (surface_instance->zs2 != NULL)
  {
    free(surface_instance->zs2);
    surface_instance->zs2 = NULL;
  }
  if (surface_instance->zt2 != NULL)
  {
    free(surface_instance->zt2);
    surface_instance->zt2 = NULL;
  }
  surface_instance->type = S_INVALID;
}


// Free memory used by instance in physicalModel/interface
void AH5_free_phm_interface_instance (AH5_interface_instance_t *interface_instance)
{
  if (interface_instance->path != NULL)
  {
    free(interface_instance->path);
    interface_instance->path = NULL;
  }
  AH5_free_opt_attrs(&(interface_instance->opt_attrs));
  if (interface_instance->medium1 != NULL)
  {
    free(interface_instance->medium1);
    interface_instance->medium1 = NULL;
  }
  if (interface_instance->medium2 != NULL)
  {
    free(interface_instance->medium2);
    interface_instance->medium2 = NULL;
  }
}

void AH5_free_phm_multilayer_instance (AH5_multilayer_instance_t *multilayer_instance)
{
  if (multilayer_instance->path != NULL)
  {
    free(multilayer_instance->path);
    multilayer_instance->path = NULL;
  }
}


// Free memory used by physicalModel category
void AH5_free_physicalmodel (AH5_physicalmodel_t *physicalmodel)
{
  hsize_t i;

  if (physicalmodel->volume_instances != NULL)
  {
    for (i = 0; i < physicalmodel->nb_volume_instances; i++)
      AH5_free_phm_volume_instance(physicalmodel->volume_instances + i);
    free(physicalmodel->volume_instances);
    physicalmodel->volume_instances = NULL;
    physicalmodel->nb_volume_instances = 0;
  }

  if (physicalmodel->surface_instances != NULL)
  {
    for (i = 0; i < physicalmodel->nb_surface_instances; i++)
      AH5_free_phm_surface_instance(physicalmodel->surface_instances + i);
    free(physicalmodel->surface_instances);
    physicalmodel->surface_instances = NULL;
    physicalmodel->nb_surface_instances = 0;
  }

  if (physicalmodel->interface_instances != NULL)
  {
    for (i = 0; i < physicalmodel->nb_interface_instances; i++)
      AH5_free_phm_interface_instance(physicalmodel->interface_instances + i);
    free(physicalmodel->interface_instances);
    physicalmodel->interface_instances = NULL;
    physicalmodel->nb_interface_instances = 0;
  }
  if (physicalmodel->multilayer_instances != NULL)
  {
    for (i = 0; i < physicalmodel->nb_multilayer_instances; i++)
      AH5_free_phm_multilayer_instance(physicalmodel->multilayer_instances + i);
    free(physicalmodel->multilayer_instances);
    physicalmodel->multilayer_instances = NULL;
    physicalmodel->nb_multilayer_instances = 0;
  }
}

void AH5_free_multiport (AH5_multiport_t *multiport) {
  if (multiport->path != NULL)
  {
    free(multiport->path);
    multiport->path = NULL;
  }
}

void AH5_free_slot (AH5_slot_t *slot) {
  if (slot->path != NULL)
  {
    free(slot->path);
    slot->path = NULL;
    AH5_free_phm_volume_instance(&(slot->material));
  }
}

char _write_first_order_filter(
    hid_t file_id, const char* path,
    const float limit, const float stat, const int nb_columns, const hsize_t nb_filters, const float* filters) {
  char success = AH5_TRUE;

  if (!AH5_write_flt_attr(file_id, path, AH5_A_ER_LIMIT, limit))
    success = AH5_FALSE;
  if (!AH5_write_flt_attr(file_id, path, AH5_A_ER_STATIC, stat))
    success = AH5_FALSE;

  if (nb_filters != 0) {
    char *path_child;
    path_child = malloc((strlen(path) + strlen(AH5_G_LIST_OF_FUNCTIONS) + 1) * sizeof(*path_child));
    strcpy(path_child, path);
    strcat(path_child, AH5_G_LIST_OF_FUNCTIONS);
      hsize_t dims[2] = {nb_filters, nb_columns};
      if (!AH5_write_flt_array(file_id, path_child, 2, dims, filters))
        success = AH5_FALSE;
    free(path_child);
  }  
  return success;
}

char AH5_write_phm_vimp(hid_t loc_id, const char *path, const AH5_material_prop_t *material_prop)
{
  char success = AH5_TRUE;
  if (strcmp(AH5_get_name_from_path(path),"relativePermittivity") == 0
    || strcmp(AH5_get_name_from_path(path),"relativePermeability") == 0)
  {
    /* relative permittivity, relative permeability */
    if (material_prop->type == MP_SINGLE_COMPLEX)
    {
      if (!AH5_write_ft_singlecomplex(loc_id, (AH5_singlecomplex_t*)&(material_prop->data.singlecomplex)))
        success = AH5_FALSE;
    }
    else if (material_prop->type == MP_ARRAYSET)
    {
      if (!AH5_write_ft_arrayset(loc_id, (AH5_arrayset_t *)&(material_prop->data.arrayset)))
         success = AH5_FALSE;
    }
    else if (material_prop->type == MP_GENERAL_RATIONAL_FUNCTION)
    {
       if (!AH5_write_ft_generalrationalfunction(loc_id, (AH5_generalrationalfunction_t *)&(material_prop->data.generalrationalfunction)))
         success = AH5_FALSE;      
    }
    else if (material_prop->type == MP_DEBYE)
    {
      hid_t group_id;
      group_id = H5Gcreate(loc_id, path, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      success = _write_first_order_filter(
              group_id, path, material_prop->data.debye.limit, material_prop->data.debye.stat, 2,
              material_prop->data.debye.nb_gtau, material_prop->data.debye.gtau);
      H5Gclose(group_id);
    }
    else if (material_prop->type == MP_LORENTZ)
    {
      hid_t group_id;
      group_id = H5Gcreate(loc_id, path, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      success = _write_first_order_filter(
              loc_id, path, material_prop->data.lorentz.limit, material_prop->data.lorentz.stat, 3,
              material_prop->data.lorentz.nb_god, material_prop->data.lorentz.god);
      H5Gclose(group_id);
    }
  }
  else
  {
    /* electric conductivity, magnetic conductivity */
        if (material_prop->type == MP_SINGLE_REAL)
        {
          if (!AH5_write_ft_singlereal(loc_id, (AH5_singlereal_t *)&(material_prop->data.singlereal)))
            success = AH5_FALSE;
        }
        else if (material_prop->type == MP_ARRAYSET)
        {
          if (!AH5_write_ft_arrayset(loc_id, (AH5_arrayset_t *)&(material_prop->data.arrayset)))
            success = AH5_FALSE;
        }
  }


  return success;
}

char AH5_write_phm_volume_instance(hid_t loc_id, const AH5_volume_instance_t *volume_instance)
{
  char success = AH5_TRUE;
  hid_t volume_instance_group_id;
  char *basename;

  basename = AH5_get_name_from_path(volume_instance->path);
  if (AH5_path_valid(loc_id, basename))
     volume_instance_group_id = H5Gopen(loc_id, basename, H5P_DEFAULT);
  else
     volume_instance_group_id = H5Gcreate(loc_id, basename, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  // Write attributes
  hsize_t i;
  for (i = 0; i < volume_instance->opt_attrs.nb_instances && success; i++)
  {
    switch (volume_instance->opt_attrs.instances[i].type)
    {
    case H5T_INTEGER:
      if (!AH5_write_int_attr(volume_instance_group_id, volume_instance->path, volume_instance->opt_attrs.instances[i].name,
                              volume_instance->opt_attrs.instances[i].value.i))
        success = AH5_FALSE;
      break;
    case H5T_FLOAT:
      if (!AH5_write_flt_attr(volume_instance_group_id, volume_instance->path, volume_instance->opt_attrs.instances[i].name,
                              volume_instance->opt_attrs.instances[i].value.f))
        success = AH5_FALSE;
      break;
    case H5T_COMPOUND:
      if (!AH5_write_cpx_attr(volume_instance_group_id, volume_instance->path, volume_instance->opt_attrs.instances[i].name,
                              volume_instance->opt_attrs.instances[i].value.c))
        success = AH5_FALSE;
      break;
    case H5T_STRING:
      if (!AH5_write_str_attr(volume_instance_group_id, volume_instance->path, volume_instance->opt_attrs.instances[i].name,
                              volume_instance->opt_attrs.instances[i].value.s))
        success = AH5_FALSE;
      break;
    default:
      success = AH5_FALSE;
      break;
    }
  }

  char *path_child;
  path_child = malloc((strlen(volume_instance->path) + strlen(AH5_G_RELATIVE_PERMITTIVITY) + 1) * sizeof(*path_child));
  strcpy(path_child, volume_instance->path);
  strcat(path_child, AH5_G_RELATIVE_PERMITTIVITY);
  if (!AH5_write_phm_vimp(volume_instance_group_id, path_child, &(volume_instance->relative_permittivity)))
    success = AH5_FALSE;
  free(path_child);

  path_child = malloc((strlen(volume_instance->path) + strlen(AH5_G_RELATIVE_PERMEABILITY) + 1) * sizeof(*path_child));
  strcpy(path_child, volume_instance->path);
  strcat(path_child, AH5_G_RELATIVE_PERMEABILITY);
  if (!AH5_write_phm_vimp(volume_instance_group_id, path_child, &(volume_instance->relative_permeability)))
    success = AH5_FALSE;

  path_child = malloc((strlen(volume_instance->path) + strlen(AH5_G_ELECTRIC_CONDUCTIVITY) + 1) * sizeof(*path_child));
  strcpy(path_child, volume_instance->path);
  strcat(path_child, AH5_G_ELECTRIC_CONDUCTIVITY);
  if (!AH5_write_phm_vimp(volume_instance_group_id, path_child, &(volume_instance->electric_conductivity)))
    success = AH5_FALSE;

  path_child = malloc((strlen(volume_instance->path) + strlen(AH5_G_MAGNETIC_CONDUCTIVITY) + 1) * sizeof(*path_child));
  strcpy(path_child, volume_instance->path);
  strcat(path_child, AH5_G_MAGNETIC_CONDUCTIVITY);
  if (!AH5_write_phm_vimp(volume_instance_group_id, path_child, &(volume_instance->magnetic_conductivity)))
    success = AH5_FALSE;

   if (!AH5_write_flt_attr(volume_instance_group_id, volume_instance->path, AH5_A_VOLUMETRIC_MASS_DENSITY,
                              volume_instance->volumetric_mass_density))
        success = AH5_FALSE;

  success &= !HDF5_FAILED(H5Gclose(volume_instance_group_id));
  return success;
}


char AH5_write_phm_surface_instance(hid_t loc_id, hid_t physical_model_id, const AH5_surface_instance_t *surface_instance) {
  char success = AH5_TRUE;
  hid_t surface_instance_group_id;
  char *basename;

  basename = AH5_get_name_from_path(surface_instance->path);
  if (AH5_path_valid(loc_id, basename))
     surface_instance_group_id = H5Gopen(loc_id, basename, H5P_DEFAULT);
  else
     surface_instance_group_id = H5Gcreate(loc_id, basename, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  // Write attributes
  hsize_t i;
  for (i = 0; i < surface_instance->opt_attrs.nb_instances && success; i++)
  {
    switch (surface_instance->opt_attrs.instances[i].type)
    {
    case H5T_INTEGER:
      if (!AH5_write_int_attr(surface_instance_group_id, surface_instance->path, surface_instance->opt_attrs.instances[i].name,
                              surface_instance->opt_attrs.instances[i].value.i))
        success = AH5_FALSE;
      break;
    case H5T_FLOAT:
      if (!AH5_write_flt_attr(surface_instance_group_id, surface_instance->path, surface_instance->opt_attrs.instances[i].name,
                              surface_instance->opt_attrs.instances[i].value.f))
        success = AH5_FALSE;
      break;
    case H5T_COMPOUND:
      if (!AH5_write_cpx_attr(surface_instance_group_id, surface_instance->path, surface_instance->opt_attrs.instances[i].name,
                              surface_instance->opt_attrs.instances[i].value.c))
        success = AH5_FALSE;
      break;
    case H5T_STRING:
      if (!AH5_write_str_attr(surface_instance_group_id, surface_instance->path, surface_instance->opt_attrs.instances[i].name,
                              surface_instance->opt_attrs.instances[i].value.s))
        success = AH5_FALSE;
      break;
    default:
      success = AH5_FALSE;
      break;
    }
  }
  
  if (surface_instance->type == S_THIN_DIELECTRIAH5_C_LAYER) {
    if (!AH5_write_flt_attr(surface_instance_group_id, surface_instance->path, AH5_A_THICKNESS, surface_instance->thickness))
        success = AH5_FALSE;
    if (!AH5_write_str_attr(surface_instance_group_id, surface_instance->path, AH5_A_PHYSICAL_MODEL, surface_instance->physicalmodel))
        success = AH5_FALSE;

    hid_t volume_instance_group_id;
    if (AH5_path_valid(physical_model_id, "volume"))
      volume_instance_group_id = H5Gopen(physical_model_id, "volume", H5P_DEFAULT);
    else
      volume_instance_group_id = H5Gcreate(physical_model_id, "volume", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
    if (!AH5_write_phm_volume_instance(volume_instance_group_id, &(surface_instance->volume_instance)))
        success = AH5_FALSE;
    
    success &= !HDF5_FAILED(H5Gclose(volume_instance_group_id));
  }
  else if (surface_instance->type == S_SIBC) {
    //Not implemented
    success = AH5_FALSE;
  }
  else if (surface_instance->type == S_ZS) {
    //Not implemented
    success = AH5_FALSE;
  }
  else if (surface_instance->type == S_ZSZT) {
    //Not implemented
    success = AH5_FALSE;
  }
  else if (surface_instance->type == S_ZSZT2) {
    //Not implemented
    success = AH5_FALSE;
  }

  success &= !HDF5_FAILED(H5Gclose(surface_instance_group_id));
  return success;
}

char AH5_write_phm_multilayer_instance(hid_t loc_id, hid_t volume_material_id, const AH5_multilayer_instance_t *multilayer_instance) {
  char success = AH5_TRUE;
  char *basename;
  hsize_t i;
  hsize_t j;
  multilayer_field* fields;
  size_t *field_offsets;
  size_t type_size;
  size_t *field_sizes;
  char **field_names;
  hid_t* field_types;
  hsize_t nb_fields;
  hid_t string_type;

  basename = AH5_get_name_from_path(multilayer_instance->path);
  
  fields = (multilayer_field *) malloc((size_t) multilayer_instance->nb_layers * sizeof(multilayer_field));
  for (i = 0; i < multilayer_instance->nb_layers; i++) {
    if (!AH5_write_phm_volume_instance(volume_material_id, &(multilayer_instance->layers[i])))
      success = AH5_FALSE;
    
    for (j = 0; j < 256; j++) {
      fields[i].path[j] = '\0';
    }
    strcat(fields[i].path, multilayer_instance->layers[i].path);
    fields[i].thickness = multilayer_instance->thicknesses[i];
  }

  nb_fields= 2;
  field_offsets = (size_t *) malloc((size_t) nb_fields * sizeof(size_t));
  field_offsets[0] = 0;
  field_offsets[1] = 256;
  field_sizes = (size_t *) malloc((size_t ) nb_fields * sizeof(size_t));
  field_sizes[0] = 256;
  field_sizes[1] = 4;
  type_size = sizeof(multilayer_field);
  field_names = (char **)malloc(nb_fields * sizeof(char *));
  field_names[0] = (char *)malloc(nb_fields * AH5_TABLE_FIELD_NAME_LENGTH * sizeof(char));
  for (i = 0; i < nb_fields; ++i) {
          field_names[i] = field_names[0] + i * AH5_TABLE_FIELD_NAME_LENGTH;
          field_names[i][0] = '\0';
  }
  strcat(field_names[0], AH5_A_PHYSICAL_MODEL);
  strcat(field_names[1], AH5_A_THICKNESS);
  field_types = (hid_t *)malloc(nb_fields * sizeof(hid_t));
  string_type = H5Tcopy( H5T_C_S1 );
  H5Tset_size(string_type, 256);
  field_types[0] = string_type;
  field_types[1] = H5T_NATIVE_FLOAT;

  if (H5TBmake_table(basename, 
                  loc_id, basename, nb_fields, multilayer_instance->nb_layers, type_size,
              (const char**)field_names, field_offsets, field_types, nb_fields, NULL, 1, fields) < 0) {
        success = AH5_FALSE;
    }

  free(field_sizes);
  free(field_offsets);   
  free(fields);
  free(field_names[0]);
  free(field_names);

  return success;
}


char AH5_write_multiport(hid_t loc_id, const AH5_multiport_t *multiport) {
  char success = AH5_TRUE;
  char *basename;
  hid_t multiport_group_id;
  hid_t multiport_rlc_group_id;
  hid_t r_group_id;
  hid_t l_group_id;
  hid_t c_group_id;
  char *path_child;

  basename = AH5_get_name_from_path(multiport->path);

  if (multiport->type == MULTIPORT_CAPACITANCE || multiport->type == MULTIPORT_RESISTANCE || 
    multiport->type == MULTIPORT_INDUCTANCE || multiport->type == MULTIPORT_IMPEDANCE) {
    if (AH5_path_valid(loc_id, basename))
      multiport_group_id = H5Gopen(loc_id, basename, H5P_DEFAULT);
    else
      multiport_group_id = H5Gcreate(loc_id, basename, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    AH5_write_str_attr(multiport_group_id, multiport->path, AH5_A_FLOATING_TYPE, AH5_V_SINGLE_REAL);
    if (multiport->type == MULTIPORT_RESISTANCE)
      AH5_write_str_attr(multiport_group_id, multiport->path, AH5_A_PHYSICAL_NATURE, AH5_A_RESISTANCE);
    else if (multiport->type == MULTIPORT_INDUCTANCE)
      AH5_write_str_attr(multiport_group_id, multiport->path, AH5_A_PHYSICAL_NATURE, AH5_A_INDUCTANCE);
    else if (multiport->type == MULTIPORT_CAPACITANCE)
      AH5_write_str_attr(multiport_group_id, multiport->path, AH5_A_PHYSICAL_NATURE, AH5_A_CAPACITANCE);
    else if (multiport->type == MULTIPORT_IMPEDANCE)
      AH5_write_str_attr(multiport_group_id, multiport->path, AH5_A_PHYSICAL_NATURE, AH5_A_IMPEDANCE);
    AH5_write_flt_attr(multiport_group_id, multiport->path, AH5_A_VALUE, multiport->value);

    success &= !HDF5_FAILED(H5Gclose(multiport_group_id));
  }
  else if (multiport->type == MULTIPORT_RLC) {

    if (AH5_path_valid(loc_id, "RLC"))
      multiport_rlc_group_id = H5Gopen(loc_id, "RLC", H5P_DEFAULT);
    else
      multiport_rlc_group_id = H5Gcreate(loc_id, "RLC", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    if (AH5_path_valid(multiport_rlc_group_id, basename))
      multiport_group_id = H5Gopen(multiport_rlc_group_id, basename, H5P_DEFAULT);
    else
      multiport_group_id = H5Gcreate(multiport_rlc_group_id, basename, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    AH5_write_int_attr(multiport_group_id, multiport->path, AH5_A_TYPE, multiport->type_rlc);

    path_child = malloc((strlen(multiport->path) + 2 + 1) * sizeof(*path_child));
    strcpy(path_child, multiport->path);
    strcat(path_child, "/R");
    r_group_id = H5Gcreate(multiport_group_id, "R", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    AH5_write_flt_attr(r_group_id, path_child, AH5_A_VALUE, multiport->r_value);
    AH5_write_str_attr(multiport_group_id, multiport->path, "R", path_child);
    free(path_child);

    path_child = malloc((strlen(multiport->path) + 2 + 1) * sizeof(*path_child));
    strcpy(path_child, multiport->path);
    strcat(path_child, "/L");
    AH5_write_str_attr(multiport_group_id, multiport->path, "L", path_child);
    l_group_id = H5Gcreate(multiport_group_id, "L", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    AH5_write_flt_attr(l_group_id, path_child, AH5_A_VALUE, multiport->l_value);
    free(path_child);

    path_child = malloc((strlen(multiport->path) + 2 + 1) * sizeof(*path_child));
    strcpy(path_child, multiport->path);
    strcat(path_child, "/C");
    AH5_write_str_attr(multiport_group_id, multiport->path, "C", path_child);
    c_group_id = H5Gcreate(multiport_group_id, "C", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    AH5_write_flt_attr(c_group_id, path_child, AH5_A_VALUE, multiport->c_value);
    free(path_child);

    success &= !HDF5_FAILED(H5Gclose(r_group_id));
    success &= !HDF5_FAILED(H5Gclose(l_group_id));
    success &= !HDF5_FAILED(H5Gclose(c_group_id));
    success &= !HDF5_FAILED(H5Gclose(multiport_rlc_group_id));
    success &= !HDF5_FAILED(H5Gclose(multiport_group_id));
  }
  else {
    AH5_log_error("Unsupported multiport type ", multiport->path);
    success = AH5_FALSE;
  }

  return success;
}

char AH5_write_slot(hid_t loc_id, hid_t volume_material_id, const AH5_slot_t *slot) {
  char success = AH5_TRUE;
  char *basename;
  hid_t slot_group_id;

  basename = AH5_get_name_from_path(slot->path);
  if (AH5_path_valid(loc_id, basename))
     slot_group_id = H5Gopen(loc_id, basename, H5P_DEFAULT);
  else
     slot_group_id = H5Gcreate(loc_id, basename, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  AH5_write_flt_attr(slot_group_id, slot->path, AH5_A_THICKNESS, slot->thickness);
  AH5_write_flt_attr(slot_group_id, slot->path, AH5_A_WIDTH, slot->width);
  AH5_write_str_attr(slot_group_id, slot->path, AH5_A_TYPE, "slot");
  AH5_write_str_attr(slot_group_id, slot->path, AH5_A_MATERIAL_MODEL, slot->material.path);

  if (!AH5_write_phm_volume_instance(volume_material_id, &(slot->material)))
       success = AH5_FALSE;

  success &= !HDF5_FAILED(H5Gclose(slot_group_id));
  return success;
}

