#include <stdlib.h>

#include <ah5.h>

#define AH5_C_PHYSICAL_MODEL_SURFACE "/physicalModel/surface"

// Read link key word
char* get_key_word(AH5_label_t *label, AH5_lnk_instance_t *lnk) {
    int id = 0;
    AH5_lbl_dataset_t *kw;
    AH5_attr_instance_t *opt = lnk->opt_attrs.instances;

    for (; opt != lnk->opt_attrs.instances + lnk->opt_attrs.nb_instances && id == -1; ++opt) {
      if (strcmp(opt->name, "subject_id") == 0) id = opt->value.i;
    }

    if (id != -1) {
      for (kw = label->datasets; kw != label->datasets + label->nb_datasets; ++kw) {
        if (strcmp(kw->path, lnk->subject) == 0) return kw->items[id];
      }
    }

    return NULL;
}

// Read real parameters from materail property
float get_real(AH5_material_prop_t *parameter) {
    if (parameter->type == MP_SINGLE_REAL) {
        return parameter->data.singlereal.value;
    } else if (parameter->type == MP_SINGLE_COMPLEX) {
        return creal(parameter->data.singlecomplex.value);
    }
    return 0.;
}

// Read Thin plane
void read_thin_dielectric_layer(
    hid_t file_id, const char *path,
    float *thickness, float *sigma, float *eps_r, float *mu_r) {
    AH5_surface_instance_t surface;
    AH5_volume_instance_t volume;

    AH5_read_phm_surface_instance(file_id, path, &surface);
    if (surface.type == S_THIN_DIELECTRIAH5_C_LAYER) {
        *thickness = surface.thickness;
        AH5_read_phm_volume_instance(file_id, surface.physicalmodel, &volume);
        *sigma = get_real(&volume.electric_conductivity);
        *eps_r = get_real(&volume.relative_permittivity);
        *mu_r = get_real(&volume.relative_permeability);
    }
}


int main(int argc, char *argv[]) {
    hid_t file_id;
    AH5_simulation_t simulation;
    AH5_lnk_group_t link;
    AH5_label_t label;
    char *path;
    char *entry_point;
    unsigned int i;
    float thickness, sigma, eps_r, mu_r;

    if (argc < 2) {
        printf("***** ERROR: Missing input filename. *****\n\n");
        return -1;
    }

    H5open();
    file_id = H5Fopen(argv[1], H5F_ACC_RDWR, H5P_DEFAULT);
    if (file_id < 0) {
        printf("***** ERROR: Cannot open file %s. *****\n\n", argv[1]);
        H5close();
        return -1;
    }

    entry_point = malloc(AH5_read_entrypoint_strlen(file_id)+1);
    AH5_read_entrypoint(file_id, entry_point);

    if (AH5_path_valid(file_id, entry_point)) {
        printf("entry point: '%s'\n", entry_point);
        if (AH5_starts_with(AH5_C_SIMULATION, entry_point)) {
            AH5_read_simulation(file_id, &simulation);
            AH5_read_label(file_id, &label);

            for (i=0; i < simulation.instances[0].nb_inputs; ++i) {
                path = simulation.instances[0].inputs[i];
                if (AH5_starts_with(AH5_C_LINK, path)) {
                    AH5_read_lnk_group(file_id, path, &link);
                }
            }

            for (i=0; i < link.nb_instances; ++i) {
                if (AH5_starts_with(AH5_C_MESH, link.instances[i].object)) {
                    if (AH5_starts_with(AH5_C_LABEL, link.instances[i].subject)) {
                        printf("mesh: '%s' - '%s'\n", link.instances[i].object, get_key_word(&label, link.instances + i));
                    } else if (AH5_starts_with(AH5_C_PHYSICAL_MODEL_SURFACE, link.instances[i].subject)) {
                        read_thin_dielectric_layer(file_id, link.instances[i].subject, &thickness, &sigma, &eps_r, &mu_r);
                        printf("mesh: '%s' - surface(e=%f, sigma=%f, eps_r=%f, mu_r=%f)\n", link.instances[i].object, thickness, sigma, eps_r, mu_r);
                    }
                }
            }

            AH5_free_lnk_group(&link);
            AH5_free_simulation(&simulation);
        }
    }

    free(entry_point);
    return 0;
}
